import React, {Component} from 'react';
import './index.less';
import {Button, Form, Input, message} from "antd";
import {LockTwoTone, UserOutlined} from '@ant-design/icons';
import Password from "antd/es/input/Password";


class Login extends Component {

    componentDidMount() {
        this.resizeHandle()
        window.addEventListener("resize", this.resizeHandle)
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.resizeHandle)
    }

    //自适应
    resizeHandle = () => {
        let window_w: number;
        const {clientHeight, clientWidth} = window.document.documentElement;
        window_w = clientWidth;
        let window_h = clientHeight;
        const realRate = window_w / window_h;
        const rate = 16 / 9;
        let fitWidth = '1920px';
        fitWidth = realRate > rate ? rate * window_h + 'px' : window_w + 'px';
        // let sizeTime = 1080;
        // let fontSize = window_h / sizeTime;
        //设置根元素的字体大小
        document.getElementsByTagName("html")[0].style.fontSize = (parseInt(fitWidth) / 1920) + 'px';
        //根据16:9设置网页的宽度
        //document.getElementsByTagName("body")[0].style.width = (window_h * 3.6) + 'px';
        document.getElementsByTagName("body")[0].style.width = fitWidth;
        const docContent = document.getElementById('content');
        if (!!docContent)
            docContent.style.height = window_h + 'px';
    }

    //登录
    submit = (values: any) => {
        if(values.username !== 'admin') {
            message.info("用户名不存在！")
            return
        }else if (values.password !== 'admin123') {
            message.info("密码错误！")
            return;
        }else if (values.username === 'admin' && values.password === 'admin123') {
            this.sleep(100).then(()=>{
                message.success("登录成功！")
            });
            this.sleep(500).then(()=> {
                sessionStorage.setItem("token", 'testToken')
                window.location.href = '/industry';
            })
        }else{
            message.info('用户名或密码错误！')
            return;
        }


    }

    //休眠
    sleep = (timeout: number) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }

    render() {
        return (
        <div id="content" className="content">
            <div className="login-main">
                <div className="title">
                    <span>跨境电商风险智能监测服务平台</span>
                </div>
                <div className="login">
                    <div className="login-title">
                        <span>账号密码登录</span>
                    </div>
                    <div className="form-style">
                        <Form onFinish={this.submit} >
                            <Form.Item
                                name="username"
                                rules={[{required: true, whitespace: true, message: '请输入用户名！'}]}>
                                <Input placeholder="用户名" prefix={<UserOutlined style={{color: '#1890FF'}}/>}/>
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[{required: true, whitespace: true, message: '请输入密码！'}]}>
                                <Password placeholder="密码" prefix={<LockTwoTone/>}/>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" block={true} htmlType={"submit"}>登录</Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
            <div className="foot-style">
                <span>copyright&copy; {new Date().getFullYear()} 上海云思智慧技术有限公司</span>
            </div>
        </div>
        );
    }
}

export default Login;
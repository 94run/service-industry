import React, {Component} from 'react';
import ReactEcharts from 'echarts-for-react'
import './index.less';


interface IndustryState {
    prodOption: any,
    riskOption: any,
    monitorOption: any,
}

class Industry extends Component {

    state: IndustryState = {
        prodOption: null,
        riskOption: null,
        monitorOption: null,
    }

    componentDidMount() {
        const token = sessionStorage.getItem("token")
        if (!!token){
            this.resizeHandle();
            window.addEventListener("resize", this.resizeHandle)
        }else{
            window.location.href = '/login'
        }

    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.resizeHandle);
    }

    //自适应
    resizeHandle = () => {
        let window_w = window.document.documentElement.clientWidth;
        let window_h = window.document.documentElement.clientHeight;
        const realRate = window_w / window_h;
        const rate = 16 / 9;
        let fitWidth = '1920px';
        if (realRate > rate) {
            fitWidth = rate * window_h + 'px';
        }else{
            fitWidth = window_w + 'px';
        }
        let sizeTime = 1080;
        let fontSize = window_h / sizeTime;
        //设置根元素的字体大小
        document.getElementsByTagName("html")[0].style.fontSize = (parseInt(fitWidth) / 1920) + 'px';
        //根据16:9设置网页的宽度
        //document.getElementsByTagName("body")[0].style.width = (window_h * 3.6) + 'px';
        // document.getElementsByTagName("body")[0].style.width = (window_h * 4.075) + 'px';
        document.getElementsByTagName("body")[0].style.width = fitWidth;
        //画线
        this.drawLine(fontSize)
    }

    //在新页面打开
    openInNewTab = (jumpUrl: string) => {
        window.open(jumpUrl)
    }

    //绘制横线
    drawLine = (fontSize: number) => {
        const canvas1=  document.getElementById("line1") as HTMLCanvasElement;
        const canvas2 = document.getElementById("line2") as HTMLCanvasElement;
        const canvas3 = document.getElementById("line3") as HTMLCanvasElement;
        const canvas4 = document.getElementById("line4") as HTMLCanvasElement;
        const canvas5 = document.getElementById("line5") as HTMLCanvasElement;
        const canvas6 = document.getElementById("line6") as HTMLCanvasElement;
        //设置长宽
        canvas1.width = 96 * fontSize;
        canvas1.height = 28.6 * fontSize;
        canvas2.width = 130 * fontSize;
        canvas2.height = 26 * fontSize;
        canvas3.width = 136 * fontSize;
        canvas3.height = 11 * fontSize;
        canvas4.width = 107 * fontSize;
        canvas4.height = 95 * fontSize;
        canvas5.width = 11 * fontSize;
        canvas5.height = 48 * fontSize;
        canvas6.width = 90 * fontSize;
        canvas6.height = 85 * fontSize;
        const ctx1=canvas1.getContext("2d");
        const ctx2=canvas2.getContext("2d");
        const ctx3=canvas3.getContext("2d");
        const ctx4=canvas4.getContext("2d");
        const ctx5=canvas5.getContext("2d");
        const ctx6=canvas6.getContext("2d");
        //绘制第一条线
        if (!!ctx1) {
            ctx1.moveTo(0,0);
            ctx1.lineTo(64 * fontSize,0);
            ctx1.lineWidth = 2
            ctx1.strokeStyle="#03BCFF";
            ctx1.stroke();
            ctx1.beginPath();
            ctx1.lineWidth = 1
            ctx1.moveTo(64 * fontSize,0);
            ctx1.lineTo(96 * fontSize,28.6 * fontSize);
            ctx1.strokeStyle="#03BCFF";
            ctx1.stroke()
        }
        // 绘制第二条线
        if (!!ctx2) {
            ctx2.moveTo(0,0);
            ctx2.lineTo(100 * fontSize,0);
            ctx2.lineWidth = 2;
            ctx2.strokeStyle="#03BCFF";
            ctx2.stroke();
            ctx2.beginPath();
            ctx2.moveTo(100 * fontSize,0);
            ctx2.lineTo(130 * fontSize,26 * fontSize);
            ctx2.lineWidth = 1
            ctx2.strokeStyle="#03BCFF";
            ctx2.stroke()
        }
        //绘制第三条线
        if (!!ctx3) {
            ctx3.moveTo(0,6);
            ctx3.lineTo(136 * fontSize,6);
            ctx3.lineWidth = 1;
            ctx3.strokeStyle="#03BCFF";
            ctx3.stroke();
        }
        // 绘制第四条线
        if (!!ctx4) {
            ctx4.beginPath();
            ctx4.moveTo(107 * fontSize,0);
            ctx4.lineTo(0, 35 * fontSize);
            ctx4.lineWidth = 1;
            ctx4.strokeStyle="#03BCFF";
            ctx4.stroke();
            ctx4.beginPath();
            ctx4.moveTo(0,35 * fontSize);
            ctx4.lineTo(0, 95 * fontSize);
            ctx4.lineWidth = 2;
            ctx4.strokeStyle="#03BCFF";
            ctx4.stroke();
        }
        // 绘制第五条线
        if (!!ctx5) {
            ctx5.beginPath();
            ctx5.moveTo(6, 0);
            ctx5.lineTo(6,48 * fontSize);
            ctx5.lineWidth = 1
            ctx5.strokeStyle = "#03BCFF";
            ctx5.stroke();
        }
        // 绘制第六条线
        if (!!ctx6) {
            ctx6.beginPath();
            ctx6.moveTo(0, 0);
            ctx6.lineTo(89 * fontSize, 35 * fontSize);
            ctx6.lineWidth = 1
            ctx6.strokeStyle = "#03BCFF"
            ctx6.stroke();
            ctx6.beginPath();
            ctx6.moveTo(89 * fontSize, 35 * fontSize);
            ctx6.lineTo(89 * fontSize , 85 * fontSize);
            ctx6.lineWidth = 2;
            ctx6.strokeStyle = "#03BCFF"
            ctx6.stroke();
        }
    }

    render() {

        const {prodOption, riskOption, monitorOption} = this.state

        return (
            <div>
                {/*  标题*/}
                <div className='contentTitle'><span>跨境电商风险智能监测服务平台</span></div>
                {/*  主要内容*/}
                <div className='main'>
                    <div className="main-top">
                        {/*  单个内容*/}
                        <div className="singleModule">
                            {/*    小标题*/}
                            <div className="littleTitle">
                                <span>商品大数据聚类系统</span>
                            </div>
                            {/*跳转按钮*/}
                            <div className="changeButton" title="点击跳转海淘首页"
    onClick={() => this.openInNewTab('http://www.baidu.com')}/>
                            {/*主要内容*/}
                            <div className="single-main">
                                <div className="single-main-left">
                                    <div className="single-main-left-item"
                                         style={{cursor: "pointer"}}
                                         onClick={()=>this.openInNewTab('http://www.baidu.com')}>
                                        <div className="left-title">
                                            <span>监控跨境电商网站</span>
                                        </div>
                                        <div className="right-number">
                                            <span>100</span>
                                            <span style={{fontSize: '30rem', marginLeft: '8rem'}}><sub>+</sub></span>
                                        </div>
                                    </div>
                                    <div className="single-main-left-item">
                                        <div className="left-title">
                                            <span>抓取商品数量</span>
                                        </div>
                                        <div className="right-number">
                                            <span>10</span>
                                            <span style={{fontSize: '30rem', marginLeft: '8rem'}}><sub>w+</sub></span>
                                        </div>
                                    </div>
                                    <div/>
                                </div>
                                <div className="single-main-right-1">
                                    {!!prodOption && <ReactEcharts
                                        option={prodOption}
                                        style={{width: '100%', height: '198rem'}}
                                    />}
                                </div>
                            </div>
                        </div>
                        <div className="singleModule">
                            {/*    小标题*/}
                            <div className="littleTitle">
                                <span>智能知识规则引擎系统</span>
                            </div>
                            <div className="changeButton" title="点击跳转规则引擎系统"
    onClick={() => this.openInNewTab('http://www.baidu.com')}/>
                            {/*主要内容*/}
                            <div className="single-main">
                                <div className="single-main-left">
                                    <div className="single-main-left-item">
                                        <div className="left-title">
                                            <span>知识规则</span>
                                        </div>
                                        <div className="right-number">
                                            <span>1</span>
                                            <span style={{fontSize: '30rem', marginLeft: '8rem'}}><sub>w+</sub></span>
                                        </div>
                                    </div>
                                    <div className="single-main-left-item">
                                        <div className="left-title">
                                            <span>智能算法</span>
                                        </div>
                                        <div className="right-number">
                                            <span>10</span>
                                            <span style={{fontSize: '30rem', marginLeft: '8rem'}}><sub>+</sub></span>
                                        </div>
                                    </div>
                                    <div/>
                                </div>
                                <div className="single-main-right-2">
                                    <div className="column" style={{marginRight: '16rem'}}>
                                        <div className="column-single">
                                            <div className="icon1"/>
                                            <div>神经网络</div>
                                        </div>
                                        <div  className="column-single">
                                            <div className="icon2"/>
                                            <div>语义理解</div>
                                        </div>
                                    </div>
                                    <div className="column" style={{marginBottom: '17rem'}}>
                                        <div  className="column-single">
                                            <div className="icon3"/>
                                            <div>海量数据处理</div>
                                        </div>
                                        <div className="icon4">
                                            <div>支持声明式、命令式编辑，兼具开发灵活、高性能</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="singleModule">
                            {/*    小标题*/}
                            <div className="littleTitle">
                                <span>风险发布服务系统</span>
                            </div>
                            <div className="changeButton" title="点击跳转风险发布系统"
    onClick={() => this.openInNewTab('http://www.baidu.com')}/>
                            {/*主要内容*/}
                            <div className="single-main">
                                <div className="single-main-left">
                                    <div className="single-main-left-item">
                                        <div className="left-title">
                                            <span>持续预警风险</span>
                                        </div>
                                        <div className="right-number">
                                            <span>600</span>
                                            <span style={{fontSize: '30rem', marginLeft: '8rem'}}><sub>天+</sub></span>
                                        </div>
                                    </div>
                                    <div className="single-main-left-item">
                                        <div className="left-title">
                                            <span>预警风险商品</span>
                                        </div>
                                        <div className="right-number">
                                            <span>13</span>
                                            <span style={{fontSize: '30rem', marginLeft: '8rem'}}><sub>w+</sub></span>
                                        </div>
                                    </div>
                                    <div/>
                                </div>
                                <div className="single-main-right-3">
                                    {!!riskOption && <ReactEcharts
                                        option={riskOption}
                                        style={{width: '100%', height: '198rem'}}
                                    />}
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="main-bottom">
                        <div className="singleModule" style={{position:'relative'}}>
                            {/*    小标题*/}
                            <div className="littleTitle">
                                <span>原厂商云</span>
                            </div>
                            <div className="changeButton" title="点击跳转原厂商云"
    onClick={() => this.openInNewTab('http://www.baidu.com')}/>
                            <div className="btn1">
                                <span>活动</span>
                                <span>小助手</span>
                            </div>
                            <div className="btn2">
                                <span>微信小程序</span>
                                <span>+</span>
                                <span>私域平台</span>
                            </div>
                            <div className="btn3">
                                <span>增值</span>
                                <span>服务</span>
                            </div>
                            <div className="btn4">
                                <span>会员</span>
                                <span>体系</span>
                            </div>
                            <div className="btn5">
                                <span>运营</span>
                                <span>工具</span>
                            </div>
                            <div className="btn6">
                                <span>个性化</span>
                                <span>服务</span>
                            </div>
                            <div className="btn7">
                                <span>精准</span>
                                <span>触达</span>
                            </div>
                        {/*    线条1*/}
                            <canvas id="line1" className="line1"/>
                            <canvas id="line2" className="line2"/>
                            <canvas id="line3" className="line3"/>
                            <canvas id="line4" className="line4"/>
                            <canvas id="line5" className="line5"/>
                            <canvas id="line6" className="line6"/>
                        </div>
                        <div className="singleModule">
                            {/*    小标题*/}
                            <div className="littleTitle">
                                <span>监管机构云</span>
                            </div>
                            <div className="changeButton" title="点击跳转监测系统"
    onClick={() => this.openInNewTab('http://www.baidu.com')}/>
                            {/*主要内容*/}
                            <div className="single-main">
                                <div className="single-main-left" style={{marginTop: '-80rem'}}>
                                    <div className="single-main-left-item"
                                    style={{cursor: "pointer"}}
                                    onClick={()=>this.openInNewTab('http://www.baidu.com')}>
                                        <div className="left-title">
                                            <span>传染病舆情</span>
                                        </div>
                                        <div className="right-number">
                                            <span>13</span>
                                            <span style={{fontSize: '30rem', marginLeft: '8rem'}}><sub>w+</sub></span>
                                        </div>
                                    </div>
                                    <div className="single-main-left-item"
                                         style={{cursor: "pointer"}}
                                         onClick={()=>this.openInNewTab('http://www.baidu.com')}>
                                        <div className="left-title">
                                            <span>监测网站</span>
                                        </div>
                                        <div className="right-number">
                                            <span>100</span>
                                            <span style={{fontSize: '30rem', marginLeft: '8rem'}}><sub>+</sub></span>
                                        </div>
                                    </div>
                                    <div className="single-main-left-item"
                                         style={{cursor: "pointer"}}
                                         onClick={()=>this.openInNewTab('http://www.baidu.com')}>
                                        <div className="left-title">
                                            <span>食品安全舆情</span>
                                        </div>
                                        <div className="right-number">
                                            <span>3</span>
                                            <span style={{fontSize: '30rem', marginLeft: '8rem'}}><sub>w+</sub></span>
                                        </div>
                                    </div>
                                    <div/>
                                </div>
                                <div className="single-main-right-5">
                                    {!!monitorOption && <ReactEcharts
                                        option={monitorOption}
                                        style={{width: '100%', height: '198rem'}}
                                    />}
                                </div>
                            </div>
                        </div>
                        <div className="singleModule">
                            {/*    小标题*/}
                            <div className="littleTitle">
                                <span>统一云平台</span>
                            </div>
                            <div className="changeButton" title="点击跳转云平台系统"
    onClick={() => this.openInNewTab('http://www.baidu.com')}/>
                            <div className="single-main-6">
                                <div className="single-main-6-top">
                                    <div className="column-single">
                                        <div className="icon1"/>
                                        <div>统一管理</div>
                                    </div>
                                    <div  className="column-single">
                                        <div className="icon2"/>
                                        <div>敏捷管理</div>
                                    </div>
                                    <div  className="column-single">
                                        <div className="icon3"/>
                                        <div>易拓展</div>
                                    </div>
                                </div>
                                <div className="single-main-6-bottom">
                                    <span>面向异构混合云环境的资源管理、运维管理、统一监控和业务系统支持平台</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Industry;
